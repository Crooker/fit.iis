<?php

function getNewPwd() {
	$chars = "abcdefghijklmnopqrstuvwxyz1234567890";
	$charslength = strlen($chars);
	$pwd = "";
	for ($i = 0; $i < 8; $i++) {
		$n = rand(0, $charslength-1);
		$pwd .= $chars[$n];
	}
	return $pwd;
}

?>