-- IIS, projekt
-- xvelec05
-- Realitni kancelar

-- Mazani tabulek
DROP TABLE IF EXISTS odpoved CASCADE;
DROP TABLE IF EXISTS dotaz CASCADE;
DROP TABLE IF EXISTS nabidka CASCADE;
DROP TABLE IF EXISTS smlouva CASCADE;
DROP TABLE IF EXISTS prohlidka CASCADE;
DROP TABLE IF EXISTS nemovitost CASCADE;
DROP TABLE IF EXISTS klient CASCADE;
DROP TABLE IF EXISTS zajemce CASCADE;
DROP TABLE IF EXISTS stav CASCADE;
DROP TABLE IF EXISTS kategorie CASCADE;
DROP TABLE IF EXISTS role CASCADE;

-- Vytvoreni tabulek

-- "enum" tabulky
CREATE TABLE stav(
  stav VARCHAR(60) UNIQUE,
  PRIMARY KEY (stav)
) COLLATE utf8_czech_ci;

CREATE TABLE kategorie(
  kategorie VARCHAR(60) UNIQUE,
  PRIMARY KEY (kategorie)
) COLLATE utf8_czech_ci;

CREATE TABLE role(
  role VARCHAR(60) UNIQUE,
  PRIMARY KEY (role)
) COLLATE utf8_czech_ci;


CREATE TABLE zajemce(
  email VARCHAR(40) UNIQUE,
  PRIMARY KEY(email)
) COLLATE utf8_czech_ci;

CREATE TABLE klient(
  kontakt VARCHAR(40) UNIQUE,
  jmeno VARCHAR(40) NOT NULL,
  prijmeni VARCHAR(40) NOT NULL,
  rodnecislo VARCHAR(10) NOT NULL,
  ulice VARCHAR (80) NOT NULL,
  mesto VARCHAR (40) NOT NULL,
  psc INT(11) NOT NULL,
  -- +420 neni typu int
  telefon VARCHAR (15) NOT NULL,
  password VARCHAR(64) NOT NULL,
  role VARCHAR(60) NOT NULL,
  FOREIGN KEY (kontakt) REFERENCES zajemce(email),
  FOREIGN KEY (role) REFERENCES role(role),
  PRIMARY KEY (kontakt)
) COLLATE utf8_czech_ci;

CREATE TABLE nemovitost(
  ident INT AUTO_INCREMENT,
  majitel VARCHAR(40),
  kategorie VARCHAR(60) NOT NULL DEFAULT 'dům',
  -- pr 3+kk
  velikost VARCHAR(10),
  -- rozloha zastavene plochy
  rozloha INT,  
  -- rozloha pozemku
  pozemek INT,
  cena INT,
  ulice VARCHAR(80) NOT NULL,
  mesto VARCHAR(40) NOT NULL,
  psc INT NOT NULL,
  stav VARCHAR(60) DEFAULT 'na prodej' NOT NULL,
  FOREIGN KEY (majitel) REFERENCES klient(kontakt),
  FOREIGN KEY (kategorie) REFERENCES kategorie(kategorie),
  FOREIGN KEY (stav) REFERENCES stav(stav),
  PRIMARY KEY(ident)
) COLLATE utf8_czech_ci;

CREATE TABLE nabidka(
  ident INT AUTO_INCREMENT,
  cena INT NOT NULL,
  kdo VARCHAR(40) NOT NULL,
  naco INT NOT NULL,
  aktualni BOOLEAN DEFAULT 1 NOT NULL,
  FOREIGN KEY (kdo) REFERENCES zajemce(email),
  FOREIGN KEY (naco) REFERENCES nemovitost(ident),
  PRIMARY KEY(ident)
) COLLATE utf8_czech_ci;

CREATE TABLE prohlidka(
  ident INT AUTO_INCREMENT,
  datumcas TIMESTAMP NOT NULL DEFAULT 0,
  kdo VARCHAR(40) NOT NULL,
  ceho INT NOT NULL,
  aktualni BOOLEAN DEFAULT 1 NOT NULL,
  FOREIGN KEY (kdo) REFERENCES zajemce(email),
  FOREIGN KEY (ceho) REFERENCES nemovitost(ident),
  PRIMARY KEY(ident)
) COLLATE utf8_czech_ci;


CREATE TABLE dotaz(
  ident INT AUTO_INCREMENT,
  kdo VARCHAR(40) NOT NULL,
  naco INT NOT NULL,
  FOREIGN KEY (kdo) REFERENCES zajemce(email),
  FOREIGN KEY (naco) REFERENCES nemovitost(ident),
  datum TIMESTAMP NOT NULL DEFAULT 0,
  obsah VARCHAR(500) NOT NULL,
  PRIMARY KEY(ident)
) COLLATE utf8_czech_ci;

CREATE TABLE odpoved(
  obsah VARCHAR(500) NOT NULL,
  datum TIMESTAMP NOT NULL DEFAULT 0,
  dotaz INT NOT NULL,
  FOREIGN KEY (dotaz) REFERENCES dotaz(ident) ON DELETE CASCADE
) COLLATE utf8_czech_ci;

CREATE TABLE smlouva(
  ident INT AUTO_INCREMENT,
  datum DATE NOT NULL DEFAULT 0,
  cena INT NOT NULL,
  prodavajici VARCHAR(40) NOT NULL,
  kupujici VARCHAR(40) NOT NULL,
  nemov INT NOT NULL,
  FOREIGN KEY (prodavajici) REFERENCES klient(kontakt),
  FOREIGN KEY (kupujici) REFERENCES klient(kontakt),
  FOREIGN KEY (nemov) REFERENCES nemovitost(ident),
  PRIMARY KEY(ident)
) COLLATE utf8_czech_ci;