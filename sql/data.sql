DELETE FROM odpoved;
DELETE FROM dotaz;
DELETE FROM nabidka;
DELETE FROM smlouva;
DELETE FROM prohlidka;
DELETE FROM nemovitost;
DELETE FROM klient;
DELETE FROM zajemce;

ALTER TABLE nemovitost AUTO_INCREMENT = 1;
ALTER TABLE smlouva AUTO_INCREMENT = 1;
ALTER TABLE nabidka AUTO_INCREMENT = 1;
ALTER TABLE prohlidka AUTO_INCREMENT = 1;
ALTER TABLE dotaz AUTO_INCREMENT = 1;

INSERT INTO zajemce VALUES('admin@realitka.iis');
INSERT INTO zajemce VALUES('Culibrk@seznam.cz');
INSERT INTO zajemce VALUES('dlouhy@seznam.cz');
INSERT INTO zajemce VALUES('Novotny@gmail.com');
INSERT INTO zajemce VALUES('shrek@fit.vutrb.cz');
INSERT INTO zajemce VALUES('chytry@gmail.com');
INSERT INTO zajemce VALUES('hloupy@gmail.com');
INSERT INTO zajemce VALUES('kratka@gmail.com');
INSERT INTO zajemce VALUES('smutna@gmail.com');
INSERT INTO zajemce VALUES('zekon@volny.cz');
INSERT INTO zajemce VALUES('phreak@riotgames.com');

INSERT INTO klient VALUES ('admin@realitka.iis','Lukáš','Velecký','9311225000','Drslavice 106','Drslavice','68733','734678678',SHA1("admin"), "admin");
INSERT INTO klient VALUES ('Culibrk@seznam.cz','Marek','Staňa','930321111','Volna 12','Vlčnov','68547','604599787', SHA1("zamestnanec"), "zaměstnanec");
INSERT INTO klient VALUES ('dlouhy@seznam.cz','Petr','Dlouhý','9031111111','Dlouhá','Brno','68547','123456789', SHA1("klient"), "klient");
INSERT INTO klient VALUES ('Novotny@gmail.com','Jiří','Novotný','9505136245','Široká','Praha','25201','333101666', SHA1("klient"), "klient");
INSERT INTO klient VALUES ('shrek@fit.vutrb.cz','Shrek','The Ogre','9501010101','Božetěchova','Brno','68547','604599787', SHA1("klient"), "klient");
INSERT INTO klient VALUES ('chytry@gmail.com','Jiří','Chytrý','8711222223','Divná','Útěchov','93939','903666666', SHA1("klient"), "klient");
INSERT INTO klient VALUES ('hloupy@gmail.com','Petr','Hloupy','8001107422','Komenského 169','Uherský Brod','68801','572632202', SHA1("klient"), "klient");
INSERT INTO klient VALUES ('kratka@gmail.com','Krátka', 'Aneta','4261789015','Reissigova','Brno','68547','754245687', SHA1("klient"), "klient");

INSERT INTO nemovitost VALUES (DEFAULT,'dlouhy@seznam.cz', 'byt','3+1',55,55,1000000,'Boženy Němcové 25','Praha 4','12105','na prodej');
INSERT INTO nemovitost VALUES (DEFAULT,'hloupy@gmail.com', 'byt','9+2',460,1000,10000000,'Kapitánka 5','Slavkov','58512','na prodej');
INSERT INTO nemovitost VALUES (DEFAULT,'Culibrk@seznam.cz', 'dům','7+2',360,720,6500000,'Kapitánka 9','Slavkov','58512','na prodej');
INSERT INTO nemovitost VALUES (DEFAULT,'Novotny@gmail.com', 'pozemek','',0,2000,1000000,'Louky 2','Lopeník','68925','prodane');
INSERT INTO nemovitost VALUES (DEFAULT,'chytry@gmail.com', 'komerční prostor','1+1',120,120,5000000,'Náměstí Svobody 1','Brno','68547','na prodej');
INSERT INTO nemovitost VALUES (DEFAULT,'admin@realitka.iis', 'garáže a jiné','1',24,32,455000,'U rybníka','Uherský Brod','68801','na prodej');
INSERT INTO nemovitost VALUES (DEFAULT,'shrek@fit.vutrb.cz', 'byt','1+kk',16,16,2000000,'Božetěchova','Brno','68547','na prodej');

INSERT INTO smlouva VALUES (DEFAULT, '15-05-2012 00:00:00', 700000, 'chytry@gmail.com', 'Culibrk@seznam.cz', 5);
INSERT INTO smlouva VALUES (DEFAULT, '10-05-2013 00:00:00', 500000, 'Culibrk@seznam.cz', 'admin@realitka.iis', 3);
INSERT INTO smlouva VALUES (DEFAULT, '20-06-2013 00:00:00', 5000000, 'hloupy@gmail.com', 'chytry@gmail.com', 2);
INSERT INTO smlouva VALUES (DEFAULT, '20-06-2013 00:00:00', 4000000, 'admin@realitka.iis', 'chytry@gmail.com', 3);
INSERT INTO smlouva VALUES (DEFAULT, '01-10-2013 00:00:00', 1000000, 'Novotny@gmail.com', 'hloupy@gmail.com', 4);
INSERT INTO smlouva VALUES (DEFAULT, '07-02-2014 00:00:00', 850000, 'admin@realitka.iis', 'Culibrk@seznam.cz', 6);

INSERT INTO nabidka VALUES (DEFAULT, 8000000, 'Culibrk@seznam.cz', 2, 1);
INSERT INTO nabidka VALUES (DEFAULT, 5000000, 'Culibrk@seznam.cz', 3, 1);
INSERT INTO nabidka VALUES (DEFAULT, 8500000, 'admin@realitka.iis', 2, 1);
INSERT INTO nabidka VALUES (DEFAULT, 5500000, 'admin@realitka.iis', 3, 1);
INSERT INTO nabidka VALUES (DEFAULT, 9000000, 'Culibrk@seznam.cz', 2, 1);

INSERT INTO prohlidka VALUES (DEFAULT, '2017-03-30 14:00:00', 'smutna@gmail.com', 2, 1);
INSERT INTO prohlidka VALUES (DEFAULT, '2017-03-30 15:00:00', 'smutna@gmail.com', 3, 1);
INSERT INTO prohlidka VALUES (DEFAULT, '2016-12-11 15:25:00', 'admin@realitka.iis', 2, 1);
INSERT INTO prohlidka VALUES (DEFAULT, '2016-11-25 08:30:00', 'hloupy@gmail.com', 6, 1);
INSERT INTO prohlidka VALUES (DEFAULT, '2017-04-01 10:15:00', 'shrek@fit.vutrb.cz', 6, 1);

INSERT INTO dotaz VALUES (DEFAULT, 'smutna@gmail.com', 2, '2017-02-25 16:25:45', 'Nějaký užitečný text');
INSERT INTO dotaz VALUES (DEFAULT, 'smutna@gmail.com', 3, '2017-02-25 16:28:28', 'Další zajímavý text');
INSERT INTO dotaz VALUES (DEFAULT, 'kratka@gmail.com', 5, '2017-01-16 10:59:31', 'Nějaký zajímavá otázka');
INSERT INTO dotaz VALUES (DEFAULT, 'kratka@gmail.com', 6, '2017-09-15 10:59:05', 'Tahle zpráva je naprostá ztráta času');

INSERT INTO odpoved VALUES ('Ano, je.', '2017-09-16 01:11:31', 4);
INSERT INTO odpoved VALUES ('Další ztracený čas', '2017-09-18 02:14:', 4);
INSERT INTO odpoved VALUES ('Nějaká zajimává odpověď.', '2017-02-16 13:46:57', 3);