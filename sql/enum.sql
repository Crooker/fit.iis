DELETE FROM stav;
ALTER TABLE stav AUTO_INCREMENT = 1;

DELETE FROM kategorie;
ALTER TABLE kategorie AUTO_INCREMENT = 1;

DELETE FROM role;
ALTER TABLE role AUTO_INCREMENT = 1;

-- Data pro "enum" tabulky
INSERT INTO stav VALUES ("na prodej"), ("prodané");
INSERT INTO kategorie VALUES ("dům"), ("byt"), ("pozemek"), ("komerční prostor"), ("garáže a jiné");
INSERT INTO role VALUES ("admin"), ("zaměstnanec"), ("klient");