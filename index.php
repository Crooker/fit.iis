<?php 

include "php/connection.php";
include "php/login.php";
include "php/passwordgenerator.php";

header('Content-Type: text/html; charset=UTF-8');
$conn = new connection();
$conn->openDbConnection();

if (!empty($_POST)) {
	if(isset($_POST["login_button"])) {
		logInUser($_POST["login_name"], $_POST["login_password"], $conn);
	}
	else if(isset($_POST["logout_button"])) {
		logoutUser();
	}
}
?>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8"> 
	<meta name="author" content="xvelec05">
	<meta name="keywords" content="">
	<link rel="stylesheet" href="style/style.css" type="text/css">

	<title>Realitní kancelář</title>

	</head>

	<body>
		<header>
			<?php include "content/login.php" ?>
		</header>

		<nav>
			<ul>
				<li><a href="?page=home">Domů</a></li>
				<li><a href="?page=klienti">Seznam klientů</a></li>
				<li><a href="?page=nemovitosti">Seznam nemovitostí</a></li>
				<li><a href="?page=stav">Správa stavů</a></li>
				<li><a href="?page=kategorie">Správa kategorií</a></li>
			</ul>
		</nav>

		<article>
			<?php
			if(isset($_GET['page'])) {
				if(file_exists('content/'.$_GET['page'].'.php')) {
					include "content/".$_GET['page'].".php";
				}
				else {
					include "content/error.php";
				}
			}
			else {
				include "content/home.php";
			}
			?>
		</article>

	</body>

<?php
$conn->closeDbConnection();
?>

</html>