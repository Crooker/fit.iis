<?php


//Filtr nastaven
if (isset($_POST["fbutton"])) {
	$mesto = $_POST["fmesto"];
	$kategorie = $_POST["fkategorie"];
	$velikost = $_POST["fvelikost"];
	$rozlohadown = $_POST["frozlohadown"];
	$rozlohaup = $_POST["frozlohaup"];
	$cenadown = $_POST["fcenadown"];
	$cenaup = $_POST["fcenaup"];
	$stav = $_POST["fstav"];
}
//Default filtr
else {
	$mesto = "";
	$kategorie = "vse";
	$velikost = "";
	$rozlohadown = "";
	$rozlohaup = "";
	$cenadown = "";
	$cenaup = "";
	$stav = "vse";
}

$nemsql = "SELECT ident, kategorie, velikost, mesto, cena FROM nemovitost";

$where = false;

if ($mesto != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " mesto = '".$mesto."'";
}

if (!($kategorie == "" || $kategorie == "vse")) {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " kategorie = '".$kategorie."'";
}

if ($velikost != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " velikost = '".$velikost."'";
}

if ($rozlohadown != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " rozloha > ".$rozlohadown;
}

if ($rozlohaup != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " rozloha < ".$rozlohaup;
}

if ($cenadown != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " cena > ".$cenadown;
}

if ($cenaup != "") {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " cena < ".$cenaup;
}

if (!($stav == "" || $stav == "vse")) {
	if (!($where)) {
		$where = true;
		$nemsql .= " WHERE";
	}
	else {
		$nemsql .= " AND";
	}
	$nemsql .= " stav = '".$stav."'";
}

$nemsql .= " ORDER BY cena ASC";

$nemresult = mysql_query($nemsql);
echo mysql_error();

$katsql = "SELECT * FROM kategorie";
$katresult = mysql_query($katsql);

$stavsql = "SELECT * FROM stav";
$stavresult = mysql_query($stavsql);
?>

<form method="POST" class="nemovitosti">
	<table class="filter">
		<tr>
			<td>Město:</td>
			<td><input type="text" name="fmesto" value="<?php echo $mesto?>"></td>
		</tr>
		<tr>
			<td>Kategorie:</td>
			<td>
				<select name="fkategorie">
					<option value="vse" <?php if ("vse" == $stav) echo "selected"; ?>>Vše</option>
					<?php
					while($katinfo = mysql_fetch_assoc($katresult)) {
						?>
						<option value="<?php echo $katinfo["kategorie"]; ?>" <?php if ($katinfo["kategorie"] == $kategorie) echo "selected"; ?> ><?php echo $katinfo["kategorie"]; ?></option>
						<?php
					}
					?>
				</select>
			</td>
		<tr>
			<td>Velikost:</td>
			<td><input type="text" name="fvelikost" value="<?php echo $velikost?>" placeholder="3+1"></td>
		</tr>
		<tr>
			<td>Rozloha - dolní mez:</td>
			<td><input type="text" name="frozlohadown" value="<?php echo $rozlohadown?>"></td>
		</tr>
		<tr>
			<td>Rozloha - horní mez:</td>
			<td><input type="text" name="frozlohaup" value="<?php echo $rozlohaup?>"></td>
		</tr>
		<tr>
			<td>Cena - dolní mez: </td>
			<td><input type="text" name="fcenadown" value="<?php echo $cenadown?>"></td>
		</tr>
		<tr>
			<td>Cena - horní mez:</td>
			<td><input type="text" name="fcenaup" value="<?php echo $cenaup?>"></td>
		</tr>
		<tr>
			<td>Stav:</td>
			<td>
				<select name="fstav">
					<option value="vse" <?php if ("vse" == $stav) echo "selected"; ?>>Vše</option>
					<?php
					while($stavinfo = mysql_fetch_assoc($stavresult)) {
						?>
						<option value="<?php echo $stavinfo["stav"]; ?>" <?php if ($stavinfo["stav"] == $stav) echo "selected"; ?> ><?php echo $stavinfo["stav"]; ?></option>
						<?php
					}
					?>				
				</select>
			</td>
		<tr>
			<td></td>
			<td><button name="fbutton">Vyhledat!</button></td>
		</tr>
	</table>
</form>

<table class="nemovitosti">
<?php
while($neminfo = mysql_fetch_assoc($nemresult)) {
	?>
	<tr>
		<th><a href="?page=nemovitost&nemid=<?php echo $neminfo["ident"]?>"><?php echo $neminfo["mesto"]; ?></a></th>
		<td><?php echo $neminfo["kategorie"]; ?></td>
		<td><?php echo $neminfo["velikost"]; ?></td>
		<td><?php echo $neminfo["cena"].",-"; ?></td>
	</tr>
	<?php
}
?>
</table>

<?php
if (isset($_COOKIE["IISLogged"])) {
	if ($_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec") {
		?>
		<form method="GET" class="odstup">
			<input type="hidden" name="page" value="upravitnemovitost">
			<input type="hidden" name="novy">
			<button>Nová nemovitost!</button>

		</form>
		<?php
	}
}
?>