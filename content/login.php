<?php

if(isset($_COOKIE["IISLogged"])) {
	?>
	<div id="logged">
		<p>
			<a href="?page=klient&userid=<?php echo $_COOKIE["IISLogged"]; ?>"><?php echo $_COOKIE["IISLogged"]; ?></a>
			<form method="POST">
				<button name="logout_button">Odhlásit</button>
			</form>
		</p>
	</div>
	<?php
}
else {
	?>
	<div id="login_form">
		<form method="POST">
			<input type="text" class="" name="login_name" placeholder="login" required>
			<input type="password" class="" name="login_password" placeholder="heslo" required>
			<button name="login_button">Přihlásit</button>
		</form>
	</div>
	<?php
}