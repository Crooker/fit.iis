<?php
if (!isset($_COOKIE["IISRole"])) {
	echo "Nemáte opravnění k zobrazení této stránky!";
	return;
}

elseif (!($_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec")) {
	echo "Nemáte opravnění k zobrazení této stránky!";
	return;
}

if (!isset($_GET["nemid"]) && !isset($_GET["novy"])) {
	echo "Nezadána nemovitost!";
	return;
}

if (isset($_POST["nbutton"])){
	$nemid = $_POST["nid"];
	$majitel = $_POST["nmajitel"];
	$kategorie = $_POST["nkategorie"];
	$velikost = $_POST["nvelikost"];
	$rozloha = $_POST["nrozloha"];
	$pozemek = $_POST["npozemek"];
	$cena = $_POST["ncena"];
	$ulice = $_POST["nulice"];
	$mesto = $_POST["nmesto"];
	$psc = $_POST["npsc"];
	$stav = $_POST["nstav"];

	$buttontext = "Aktualizovat!";

	if (isset($_POST["nnovy"])){
		$klientsql = "INSERT INTO nemovitost VALUES (DEFAULT, '".$majitel."', '".$kategorie."','".$velikost."','".$rozloha."','".$pozemek."','".$cena."','".$ulice."','".$mesto."','".$psc."','".$stav."')";
		$outputmessage = "<p>Nemovitost vytvořena!</p>";
	}
	else {
		$klientsql = "UPDATE nemovitost SET majitel='".$majitel."', kategorie='".$kategorie."', velikost='".$velikost."', rozloha='".$rozloha."', pozemek='".$pozemek."', cena='".$cena."', ulice='".$ulice."', mesto='".$mesto."', psc='".$psc."', stav='".$stav."' WHERE ident='".$nemid."';";
		$outputmessage = "<p>Nemovitost upravena!</p>";
	}

	if(!(mysql_query($klientsql))) echo mysql_error();
	else echo $outputmessage;

	if (isset($_POST["nnovy"])) {
		$nemidsql = "SELECT ident FROM nemovitost ORDER BY ident DESC LIMIT 1";
		if (!($nemidresult = mysql_query($nemidsql))) echo mysql_error();
		$nemidinfo = mysql_fetch_assoc($nemidresult);
		$nemid = $nemidinfo["ident"];
	}

}

elseif (!isset($_GET["novy"])) {
	$nemsql = "SELECT * FROM nemovitost WHERE ident = '".$_GET["nemid"]."'";
	if (!($nemresult = mysql_query($nemsql))) echo mysql_error();
	$neminfo = mysql_fetch_assoc($nemresult);

	$nemid = $_GET["nemid"];
	$majitel = $neminfo["majitel"];
	$kategorie = $neminfo["kategorie"];
	$velikost = $neminfo["velikost"];
	$rozloha = $neminfo["rozloha"];
	$pozemek = $neminfo["pozemek"];
	$cena = $neminfo["cena"];
	$ulice = $neminfo["ulice"];
	$mesto = $neminfo["mesto"];
	$psc = $neminfo["psc"];
	$stav = $neminfo["stav"];
	$buttontext = "Aktualizovat!";
}

else {
	$nemid = "";
	$majitel = "";
	$kategorie = "";
	$velikost = "";
	$rozloha = "";
	$pozemek = "";
	$cena = "";
	$ulice = "";
	$mesto = "";
	$psc = "";
	$stav = "na prodej";
	$buttontext = "Vytvořit!";
}

$stavsql = "SELECT * FROM stav";
if (!($stavresult = mysql_query($stavsql))) echo mysql_error();

$listklientsql = "SELECT kontakt FROM klient";
if (!($listklientresult = mysql_query($listklientsql))) echo mysql_error();

$katsql = "SELECT kategorie FROM kategorie";
if (!($katresult = mysql_query($katsql))) echo mysql_error();

?>

<form method="POST">
	<input type="hidden" name="nid" value="<?php echo $nemid?>">
	Majitel:
	<select name="nmajitel">
		<?php
		while($listklientinfo = mysql_fetch_assoc($listklientresult)) {
			?>
			<option value="<?php echo $listklientinfo["kontakt"]; ?>" <?php if ($listklientinfo["kontakt"] == $majitel) echo "selected"; ?> ><?php echo $listklientinfo["kontakt"]; ?></option>
			<?php
		}
		?>
	</select><br>

	Kategorie:
	<select name="nkategorie">
		<?php
		while($katinfo = mysql_fetch_assoc($katresult)) {
			?>
			<option value="<?php echo $katinfo["kategorie"]; ?>" <?php if ($katinfo["kategorie"] == $majitel) echo "selected"; ?> ><?php echo $katinfo["kategorie"]; ?></option>
			<?php
		}
		?>
	</select><br>

	Velikost:<input type="text" name="nvelikost" value="<?php echo $velikost; ?>"><br>
	Rozloha:<input type="text" name="nrozloha" value="<?php echo $rozloha; ?>"><br>
	Pozemek:<input type="text" name="npozemek" value="<?php echo $pozemek; ?>"><br>
	Cena:<input type="text" name="ncena" value="<?php echo $cena; ?>" required><br>
	Ulice:<input type="text" name="nulice" value="<?php echo $ulice; ?>" required><br>
	Město:<input type="text" name="nmesto" value="<?php echo $mesto; ?>" required><br>
	PSČ:<input type="text" name="npsc" value="<?php echo $psc; ?>" required><br>

	Stav:
	<select name="nstav">
		<?php
		while($stavinfo = mysql_fetch_assoc($stavresult)) {
			?>
			<option value="<?php echo $stavinfo["stav"]; ?>" <?php if ($stavinfo["stav"] == $stav) echo "selected"; ?> ><?php echo $stavinfo["stav"]; ?></option>
			<?php
		}
		?>
	</select><br>
	<?php
	if (isset($_GET["novy"])) { ?><input type="hidden" name="nnovy"><?php }
	?>
	<button name="nbutton"><?php echo $buttontext; ?></button>
</form>