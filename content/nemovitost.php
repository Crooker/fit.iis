<?php

//Neexistujici nemovitost
if (!isset($_GET["nemid"])){
	echo "Neexistujici nemovitost, akci opakujte!";
	return;
}

//Nabidnout nemovitost k prodeji
if (isset($_POST["nemtosellbutton"])) {
	$nemupdatesql = "UPDATE nemovitost SET cena=".$_POST["nemtosellprice"].", stav='na prodej' WHERE ident=".$_GET["nemid"].";";
	if (mysql_query($nemupdatesql)) {
		header("Refresh:0");
	}
	else {
		//TODO error
	}
}

//Vlozeni nabidky na nemovitost
elseif (isset($_POST["nabidkabutton"])) {
	$cena = $_POST["nabidkacena"];
	$email = $_POST["nabidkaemail"];
	$nemovitost = $_GET["nemid"];

	//zajemce existuje
	$checkexistingemailsql = "SELECT * FROM zajemce WHERE email='".$email."';";

	if(mysql_num_rows(mysql_query($checkexistingemailsql)) == 0)
	{
		$nabidkazajemcesql = "INSERT INTO zajemce VALUES ('".$email."');";
		if (!mysql_query($nabidkazajemcesql)) echo mysql_error();
	}

	$nabidkasql = "INSERT INTO nabidka VALUES (DEFAULT, '".$cena."', '".$email."', '".$nemovitost."', DEFAULT);";

	if (mysql_query($nabidkasql)) {		
		header("Refresh:0");
	}
	else {
		echo mysql_error();
	}
}

//Vlozeni prohlidky na nemovitost
elseif (isset($_POST["prohlidkabutton"])) {
	$cas = $_POST["prohlidkacas"];
	$email = $_POST["prohlidkaemail"];
	$nemovitost = $_GET["nemid"];

	$datumcas = date('Y-m-d H:i:s', strtotime($cas));

	//zajemce existuje
	$checkexistingemailsql = "SELECT * FROM zajemce WHERE email='".$email."';";

	if(mysql_num_rows(mysql_query($checkexistingemailsql)) == 0)
	{
		$prohlidkazajemcesql = "INSERT INTO zajemce VALUES ('".$email."');";
		if (!mysql_query($prohlidkazajemcesql)) echo mysql_error();
	}

	$prohlidkasql = "INSERT INTO prohlidka VALUES (DEFAULT, '".$datumcas."', '".$email."', '".$nemovitost."', DEFAULT);";
	if (mysql_query($prohlidkasql)) {		
		header("Refresh:0");
	}
	else {
		echo mysql_error();
	}
}

// Zobrazeni info k nemovitosti
$nemsql = "SELECT * FROM nemovitost WHERE ident = '".$_GET["nemid"]."'";
$nemresult = mysql_query($nemsql);
$neminfo = mysql_fetch_assoc($nemresult);
?>

<div class="nemovitost">
	<table>
		<tr>
			<th>Kategorie:</th>
			<td><?php echo $neminfo["kategorie"]; ?></td>
		</tr>
		<tr>
			<th>Velikost:</th>
			<td><?php echo $neminfo["velikost"]; ?></td>
		</tr>
		<tr>
			<th>Rozloha:</th>
			<td><?php echo $neminfo["rozloha"]; ?>m<sup>2</sup>,</td>
		</tr>
		<tr>
			<th>Pozemek:</th>
			<td><?php echo $neminfo["pozemek"]; ?>m<sup>2</sup></td>		
		</tr>
		<tr>
			<th>Cena:</th>
			<td><?php echo $neminfo["cena"]; ?>,- Kč, </td>
		</tr>
		<tr>
			<th>Stav:</th>
			<td><?php echo $neminfo["stav"]; ?></td>		
		</tr>
		<tr>
			<th>Ulice:</th>
			<td><?php echo $neminfo["ulice"]; ?></td>
		</tr>
		<tr>
			<th>Město:</th>
			<td><?php echo $neminfo["mesto"]; ?></td>
		</tr>
		<tr>
			<th>PSČ:</th>
			<td><?php echo $neminfo["psc"]; ?></td>
		</tr>
	</table>
</div>

<?php

// Administrativa pro majitele nemovitosti nebo zamestnance
if (isset($_COOKIE["IISLogged"])) {
	if ($_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec") {
			?>
			<p>
				<a href="?page=upravitnemovitost&nemid=<?php echo $_GET["nemid"]?>">Upravit nemovitost</a>
			</p>
			<p>
				<a href="?page=smlouvy&nemid=<?php echo $_GET["nemid"]?>">Zobrazit smlouvy nemovitosti</a>
			</p>
			<?php
		}

	if (($_COOKIE["IISLogged"] == $neminfo["majitel"]) || $_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec") {
		if ($neminfo["stav"] == "prodané") {
			?>
			<form method="POST">
				<span>Cena: </span><input type="text" name="nemtosellprice" required>
				<button name="nemtosellbutton">Prodat nemovitost</button>
			</form>
			<?php
		}
	}

	if ($neminfo["stav"] == "na prodej") {
		if (($_COOKIE["IISLogged"] == $neminfo["majitel"]) || $_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec") {
			
			//Zobrazit nabidky na nemovitost
			$nabidkysql = "SELECT * FROM nabidka WHERE naco = '".$_GET["nemid"]."' AND aktualni = '1';";
			$nabidkyresult = mysql_query($nabidkysql);
			?>
			<h3>Nabídky na nemovitost</h3>
			<form method="POST" action="?page=novasmlouva">
			<?php
			while($nabidkainfo = mysql_fetch_assoc($nabidkyresult)) {
				?>
				<p>
					<input type="hidden" name="nabidkanemid" value="<?php echo $_GET["nemid"];?>">
					<input type="hidden" name="nabidkamajitel" value="<?php echo $neminfo["majitel"];?>">
					<input type="text" name="nabidkakupujici" value="<?php echo $nabidkainfo["kdo"];?>" readonly>
					<input type="text" name="nabidkacena" value="<?php echo $nabidkainfo["cena"];?>" readonly>
					<?php
					$existujeklientsql = "SELECT kontakt FROM klient WHERE kontakt = '".$nabidkainfo["kdo"]."'";
					$existujeklientresult = mysql_query($existujeklientsql);

					if ($_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec") {
						if (mysql_num_rows($existujeklientresult) == 1) {
							?>
							<button name="nabidkabutton">Smlouva</button>
							<?php
						}
						else {
							?>
							<a href="?page=upravitklienta&novy&email=<?php echo $nabidkainfo["kdo"];?>">Vytvořit klienta!</a>
							<?php
						}
					}
					?>					
				</p>
				<?php
			}
			?>
			</form>			
			<?php

			//Zobrazit prohlidky
			$prohlidkysql = "SELECT datumcas, kdo FROM prohlidka WHERE ceho = '".$_GET["nemid"]."' AND prohlidka.aktualni = '1';";
			$prohlidkyresult = mysql_query($prohlidkysql);			
			?>
			<h3>Prohlídky na nemovitost</h3>
			<table id="prohlidky">
			<?php
			while($prohlidkainfo = mysql_fetch_assoc($prohlidkyresult)) {
				?>
				<tr>
					<th><?php echo $prohlidkainfo["kdo"]?></a></th>
					<td><?php echo $prohlidkainfo["datumcas"]?></td>
				</tr>
				<?php
			}
			?>
			</table>			
			<?php
		}
	}
}

// Informace pro zajemce o nemovitost
if ($neminfo["stav"] == "na prodej") {
	
	if (isset($_COOKIE["IISLogged"])) $username = $_COOKIE["IISLogged"];
	else $username = "";

	// Formular k nabidce na nemovitost
	if (!($username == $neminfo["majitel"])) {
		?>
		<h3>Nabídka na nemovitost</h3>
		<form method="POST">
			<span>Email: </span><input type="email" name="nabidkaemail" required value="<?php echo $username; ?>"><br>
			<span>Nabídnutá cena: </span><input type="text" name="nabidkacena" required><br>
			<button name="nabidkabutton">Podat nabídku</button>
		</form>
	<?php
	}

	// Formular k zadosti o prohlidku
	if (!($username == $neminfo["majitel"])) {
		?>
		<h3>Žádost o prohlídku</h3>
		<form method="POST">
			<span>Email: </span><input type="text" name="prohlidkaemail" required value="<?php echo $username; ?>"><br>
			<span>Den a čas prohlídky: </span><input type="text" name="prohlidkacas" required><span class="formnote">(1.1.2001 14:00 nebo 01.01.2001 9:00)</span><br>
			<button name="prohlidkabutton">Žádost o prohlídku</button>
		</form>
	<?php
	}
}


// Dotazy k nemovitosti

$dotazysql = "SELECT * FROM dotaz WHERE naco = '".$_GET["nemid"]."'";
$dotazyresult = mysql_query($dotazysql);
?>

<h3>Dotazy k nemovitosti</h3>
<table>

<?php
while($dotazyinfo = mysql_fetch_assoc($dotazyresult)) {
	?>
	<tr>
		<th><?php echo $dotazyinfo["kdo"]?></th>
		<th><?php echo $dotazyinfo["datum"]?></th>
		<th>
			<form method="GET">
				<input type="hidden" name="page" value="odpoved">
				<input type="hidden" name="did" value="<?php echo $dotazyinfo["ident"]?>">
				<input type="hidden" name="nemid" value="<?php echo $_GET["nemid"]?>">
				<button name="dbutton">Odpovědět!</button>
			</form>
		</th>
	</tr>
	<tr>
		<td colspan="3"><?php echo $dotazyinfo["obsah"]?></td>
	</tr>
	<?php
	$odpovedsql = "SELECT * FROM odpoved WHERE dotaz = '".$dotazyinfo["ident"]."'";
	$odpovedresult = mysql_query($odpovedsql);
	while($odpovedinfo = mysql_fetch_assoc($odpovedresult)) {
		?>
		<tr>
			<th></th>
			<th>Odpověď:</th>
			<th><?php echo $odpovedinfo["datum"]?></th>
		</tr>
		<tr>
			<td></td>
			<td colspan="2"><?php echo $odpovedinfo["obsah"]?></td>
		</tr>
		<?php
	}

}
?>

</table>

<a href="?page=dotaz&nemid=<?php echo $_GET["nemid"];?>">Položit dotaz!</a>