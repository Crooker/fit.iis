<?php
if (!isset($_COOKIE["IISRole"])) {
	echo "Nemáte opravnění k zobrazení této stránky!";
	return;
}

elseif (!($_COOKIE["IISRole"] == "admin" || $_COOKIE["IISRole"] == "zaměstanec")) {
	echo "Nemáte opravnění k zobrazení této stránky!";
	return;
}

if(!isset($_GET["sid"])){
	echo "Smlouva nevybrána!";
	return;
}

$nemovitost = $_GET["sid"];

$smlouvasql = "SELECT s.datum AS datum, s.cena as cena, p.kontakt AS pkontakt, p.jmeno AS pjmeno, p.prijmeni AS pprijmeni, k.kontakt AS kkontakt, k.jmeno AS kjmeno, k.prijmeni AS kprijmeni, n.ident AS nident, n.kategorie AS nkategorie, n.ulice AS nulice, n.mesto AS nmesto FROM smlouva AS s INNER JOIN klient AS p ON s.prodavajici = p.kontakt INNER JOIN klient AS k ON s.kupujici = k.kontakt INNER JOIN nemovitost AS n ON s.nemov = n.ident WHERE s.ident = '".$nemovitost."';";

$smlouvaresult = mysql_query($smlouvasql);
$smlouvainfo = mysql_fetch_assoc($smlouvaresult);
?>

<div id="smlouva">
	<h3>Prodávající</h3>
	<a href="?page=klient&userid=<?php echo $smlouvainfo["pkontakt"]?>"><?php echo $smlouvainfo["pjmeno"]?> <?php echo $smlouvainfo["pprijmeni"]?></a>

	<h3>Kupující</h3>
	<a href="?page=klient&userid=<?php echo $smlouvainfo["kkontakt"]?>"><?php echo $smlouvainfo["kjmeno"]?> <?php echo $smlouvainfo["kprijmeni"]?></a>

	<h3>Nemovitost</h3>
	<a href="?page=nemovitost&nemid=<?php echo $smlouvainfo["nident"]?>"><?php echo $smlouvainfo["nkategorie"]?>, <?php echo $smlouvainfo["nulice"]?>, <?php echo $smlouvainfo["nmesto"]?></a>

	<h3>Cena</h3>
	<?php echo $smlouvainfo["cena"]?>,- Kč
</div>